# ADW Geoip Bundle #

### Installation ###

Add repository

```
"repositories": [
    {
      "type": "vcs",
      "url": "https://bitbucket.org/prodhub/adw-geoip-bundle.git"
    }
]
```

Install vendor

```
$ composer require adw/geoip-bundle dev-master
```

Enable bundle for framework

```
app/AppKernel.php
...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            ...
            new ADW\GeoIpBundle\ADWGeoIpBundle(),
            ...
        );
```

Update DB schema

```
$ php app/console doctrine:schema:update --force
```

Load data from files

```
$ php app/console adw:ipgeobase:create
```

### Usage ###

```
$location = $this->get('adw.geoip.handler')->getLocation($ip); //return recurcive Location object
$location->toArray(); //return array
$location->toJson(); //return json 

$this->get('adw.geoip.handler')->getCountry($ip);
```

### Tests ###

```
$ bin/phpunit -c vendor/adw/geoip-bundle/phpunit.xml.dist
```